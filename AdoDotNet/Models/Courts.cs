﻿using AdoDotNet.Enums;

namespace AdoDotNet.Models
{
    public class Courts
    {
        public int Id { get; set; }

        public int CourtType { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }
    }
}