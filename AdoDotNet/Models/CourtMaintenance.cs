﻿using System;

namespace AdoDotNet.Models
{
    public class CourtMaintenance
    {
        public int Id { get; set; }

        public string WorkTitle { get; set; }

        public bool CourtIsClosed { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int CourtId { get; set; }
    }
}
