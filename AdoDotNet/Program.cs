﻿using System;
using System.Collections.Generic;
using System.Data;
using AdoDotNet.Common.Common.DataLayer.SqlServer;
using AdoDotNet.Enums;
using AdoDotNet.Models;
using AdoDotNet.Sql;

namespace AdoDotNet
{
    public static class Program
    {
        public static readonly SqlServerDataManagerBase AppManager = new SqlServerDataManagerBase(Const.ConnectionString);

        public static void Main(string[] args)
        {
            //ConnectToSqlServer();
            //CommandToSqlServer();
            //DataReaderToSqlServer();
            //SqlServerExceptionManager();
            //SqlDataTable();
            //SqlDateView();
            //OwnSqlSeverDataTable();
            //SqlServerStringBuilder();

            //MultipleResultSets();
            //GetAllCourts();
            GetCourtById(15);
        }

        private static void MultipleResultSets()
        {
            AppManager.SqlCommand = "SELECT * FROM Courts; SELECT * FROM CourtMaintenance";
            using var dataReader = AppManager.GetDataReader();
            var courts = AppManager.ToList<Courts>(dataReader);
            var rows = courts.Count;

            Console.WriteLine(rows);

            dataReader.NextResult();

            var courtsMaintenance = AppManager.ToList<CourtMaintenance>(dataReader);
            rows = courtsMaintenance.Count;

            Console.WriteLine(rows);
        }

        private static void GetAllCourts()
        {
            AppManager.SqlCommand = "SELECT * FROM Courts";

            var courts = AppManager.GetRecords<Courts>(AppManager.SqlCommand);

            foreach (var court in courts)
            {
                Console.WriteLine($"Id = {court.Id}");
                Console.WriteLine($"Court type = {court.CourtType}");
                Console.WriteLine($"Name = {court.Name}");
                Console.WriteLine($"Price = {court.Price}");
                Console.WriteLine();
            }

        }

        private static void GetCourtById(int courtId)
        {
            AppManager.SqlCommand = "SELECT * FROM Courts WHERE Id = @Id";

            var parameter = new List<IDbDataParameter>()
            {
                AppManager.CreateParameter("Id", courtId, false)
            };

            var court = AppManager.GetRecord<Courts>(AppManager.SqlCommand, parameter.ToArray());

            Console.WriteLine($"Id = {court.Id}");
            Console.WriteLine($"Court type = {court.CourtType}");
            Console.WriteLine($"Name = {court.Name}");
            Console.WriteLine($"Price = {court.Price}");
        }

        private static void SqlServerStringBuilder()
        {
            var builder = new Builder();
            builder.BreakApartConnectionString();
            builder.CreateConnectionString();
            builder.CreateDataModificationCommands();
            builder.InsertDataModificationCommand();
        }

        private static void OwnSqlSeverDataTable()
        {
            var dataRowColumn = new DataRowColumn();
            dataRowColumn.BuildDataTable();
            dataRowColumn.CloneDataTable();
            dataRowColumn.CopyDataTable();
            dataRowColumn.SelectCopyRowByRow();
            dataRowColumn.SelectUsingCopyToDataTable();
        }

        private static void SqlDateView()
        {
            var view = new Sql.DataView();
            view.GetCourtsSortedByPriceDescending();
            view.GetCourtsFilteredPrice(1900);
            view.GetCourtsFilteredByLinq(2000);
            view.DataViewToDataTable();
        }

        private static void SqlDataTable()
        {
            var dataTable = new Sql.DataTable();
            dataTable.GetCourtsAsDataTable();
            dataTable.GetCourtsAsGenericList();
            dataTable.GetMultipleResult();
        }

        private static void SqlServerExceptionManager()
        {
            var exception = new ExceptionHandling();
            exception.SimpleExceptionHandling();
            exception.CatchSqlException(CourtType.Indoor, "Court 13");
            exception.GatherExceptionInformation(CourtType.Indoor, "Court 14");
        }

        private static void DataReaderToSqlServer()
        {
            var dataReader = new DataReader();
            dataReader.GetCourtsAsDataReader();
            dataReader.GetCourtMaintenanceAsGenericList();
            dataReader.GetCourtMaintenanceUsingFieldValue();
            dataReader.GetProductsUsingExtensionMethods();
            dataReader.GetMultipleResultSets();
        }

        private static void CommandToSqlServer()
        {
            var command = new Command();
            command.InsertCourt();
            command.GetCourtsCountScalar();
            command.GetCourtsCountScalarUsingParameter();
            command.InsertCourtsUsingParameters(CourtType.Indoor, "Court 9");
            command.InsertCourtOutputParameter(CourtType.Outdoor, "Court 11");
            command.TransactionProcessing(CourtType.Indoor, "Court 12");
        }

        private static void ConnectToSqlServer()
        {
            var connection = new Connection();
            connection.Connect();
        }
    }
}
