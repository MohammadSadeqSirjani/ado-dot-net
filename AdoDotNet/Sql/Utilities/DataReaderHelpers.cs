﻿using System;
using System.Data.SqlClient;

namespace AdoDotNet.Sql.Utilities
{
    public static class DataReaderHelpers
    {
        public static T GetFieldValue<T>(this SqlDataReader dataReader, string name)
        {
            return !dataReader[name].Equals(DBNull.Value) ? (T) dataReader[name] : default;
        }
    }
}