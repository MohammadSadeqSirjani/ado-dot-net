﻿using System;
using System.Data.SqlClient;
using System.Text;

namespace AdoDotNet.Sql
{
    public class Connection
    {
        private const string ConnectionString = "Server=localhost;Database=TennisBookings;Trusted_Connection=True;";

        public void Connect()
        {
            // Create SQL connection object in using block for automatic closing and disposing
            try
            {
                using var connection = new SqlConnection(ConnectionString);

                connection.Open();

                Console.WriteLine(GetConnectionInformation(connection));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Gather connection information 
        protected virtual string GetConnectionInformation(SqlConnection connection)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"Connection String: {connection.ConnectionString}");
            stringBuilder.AppendLine($"Client Connection Id: {connection.ClientConnectionId}");
            stringBuilder.AppendLine($"Packet Size: {connection.PacketSize}");
            stringBuilder.AppendLine($"Workstation Id: {connection.ClientConnectionId}");
            stringBuilder.AppendLine($"State: {connection.State}");
            stringBuilder.AppendLine($"Connection Timeout: {connection.ConnectionTimeout}");
            stringBuilder.AppendLine($"Database: {connection.Database}");
            stringBuilder.AppendLine($"Data Source: {connection.DataSource}");
            stringBuilder.AppendLine($"Server Version: {connection.ServerVersion}");
            stringBuilder.AppendLine($"Workstation Id: {connection.WorkstationId}");

            return stringBuilder.ToString();
        }
    }
}
