﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using AdoDotNet.Enums;
using AdoDotNet.Sql.SqlException;
using AdoDotNet.Sql.SqlException.Manager;

namespace AdoDotNet.Sql
{
    public class ExceptionHandling
    {
        private const string ConnectionString = "Server=localhost;Database=TennisBookings;Trusted_Connection=True;";

        public void SimpleExceptionHandling()
        {
            // Create SQL statement to submit
            const string query = "INSERT INTO Courts (Type, Name) VALUES (0, 'Court 13')";

            try
            {
                // Create a connection object in using block for automatic closing and dispoing
                using var connection = new SqlConnection(ConnectionString + ":)");
                // Open connection
                connection.Open();
                // Create a command object in using block for automatic disposal
                using var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text
                };

                // Execute an INSERT statement
                var rowsAffected = command.ExecuteNonQuery();

                Console.WriteLine($"Row affected: {rowsAffected}");
            }
            catch (Exception exception)
            {
                // NOTE: A problem here is no access to SqlCommand object
                //       Thus, we have lost the SQL statement and 
                //       connection information for reporting purpose
                Console.WriteLine(exception.Message);
            }
        }

        public void CatchSqlException(CourtType type, string name)
        {
            const string query = "sp_court_insert";
            var id = 0;

            try
            {
                using var connection = new SqlConnection(ConnectionString);
                connection.Open();
                // Create command object in using block for automatic disposal
                using var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.StoredProcedure,
                };

                // Create inputs parameters
                command.Parameters.Add(new SqlParameter("@Typee", type));
                command.Parameters.Add(new SqlParameter("@Name", name));

                // Create OUTPUT parameter
                command.Parameters.Add(new SqlParameter()
                {
                    ParameterName = "@Id",
                    Value = id,
                    DbType = DbType.Int32,
                    IsNullable = false,
                    Direction = ParameterDirection.Output
                });

                // Execute INSET statement
                var rowsAffected = command.ExecuteNonQuery();

                // Get output parameter
                id = command.Parameters["@Id"].Value as int? ?? 0;

                Console.WriteLine($"Rows affected: {rowsAffected}");
                Console.WriteLine($"Id = {id}");
            }
            catch (System.Data.SqlClient.SqlException exception)
            {
                var stringBuilder = new StringBuilder();

                for (var i = 0; i < exception.Errors.Count; i++)
                {
                    stringBuilder.AppendLine($"Index: #{i + 1}");
                    stringBuilder.AppendLine($"Type: {exception.Errors[i].GetType().FullName}");
                    stringBuilder.AppendLine($"Message: {exception.Errors[i].Message}");
                    stringBuilder.AppendLine($"Source: {exception.Errors[i].Source}");
                    stringBuilder.AppendLine($"Number: {exception.Errors[i].Number}");
                    stringBuilder.AppendLine($"State: {exception.Errors[i].State}");
                    stringBuilder.AppendLine($"Class: {exception.Errors[i].Class}");
                    stringBuilder.AppendLine($"Server: {exception.Errors[i].Server}");
                    stringBuilder.AppendLine($"Procedure: {exception.Errors[i].Procedure}");
                    stringBuilder.AppendLine($"LineNumber: {exception.Errors[i].LineNumber}");
                }

                Console.WriteLine(stringBuilder);
                Console.WriteLine(exception);
            }
        }

        public void GatherExceptionInformation(CourtType type, string name)
        {
            // Define connection/command outside of try...catch block
            // So you can gather more information for exception handling
            SqlConnection connection = null;
            SqlCommand command = null;

            try
            {
                // Create a command statement to submit
                const string query = "sp_court_insert";
                var id = 0;

                // Create SQL connection object
                connection = new SqlConnection(ConnectionString);
                // Open a connection
                connection.Open();
                // Create command object in using block for automatic disposal
                command = new SqlCommand(query, connection)
                {
                    // Set command type
                    CommandType = CommandType.StoredProcedure,
                };

                // Create inputs parameters
                command.Parameters.Add(new SqlParameter("@Typee", type));
                command.Parameters.Add(new SqlParameter("@Name", name));

                // Create OUTPUT parameter
                command.Parameters.Add(new SqlParameter()
                {
                    ParameterName = "@Id",
                    Value = id,
                    DbType = DbType.Int32,
                    IsNullable = false,
                    Direction = ParameterDirection.Output
                });

                // Execute INSET statement
                var rowsAffected = command.ExecuteNonQuery();

                // Get output parameter
                id = command.Parameters["@Id"].Value as int? ?? 0;

                Console.WriteLine($"Rows affected: {rowsAffected}");
                Console.WriteLine($"Id = {id}");
            }
            catch (System.Data.SqlClient.SqlException exception)
            {
                SqlServerExceptionManager.Instance.Publish(exception, command,
                    "Error in ExceptionHandling.GatherExceptionInformation()");
                Console.WriteLine(SqlServerExceptionManager.Instance.LastException.ToString());
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            finally
            {
                // Must close/dispose here so we have access to info for error handling
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
                command?.Dispose();
            }
        }
    }
}
