﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AdoDotNet.Sql
{
    public class DataRowColumn
    {
        // Create connection string
        private const string ConnectionString =
            "Server=localhost;Database=TennisBookings;Trusted_Connection=True;";

        public System.Data.DataTable BuildDataTable()
        {
            var dataTable = new System.Data.DataTable();

            /* Create a column */

            // Method 1: Use Add method to build a column
            dataTable.Columns.Add("Id", typeof(int));

            // Method 2: Create a DataColumn object for more control
            var dataColumn = new DataColumn()
            {
                DataType = typeof(string),
                ColumnName = "Name",
                Caption = "Court Name",
                ReadOnly = true
            };
            dataTable.Columns.Add(dataColumn);

            // Method 3: Create a DataColumn object in Add() method
            dataTable.Columns.Add(new DataColumn()
            {
                DataType = typeof(int),
                ColumnName = "Type",
                Caption = "Court Type",
                ReadOnly = false
            });

            dataTable.Columns.Add(new DataColumn()
            {
                DataType = typeof(decimal),
                ColumnName = "Price",
                Caption = "Court Price",
                ReadOnly = true
            });

            /* Create a row */

            // Method 1: Pass in variable amount of argument to Add() method
            // Pass data in the same order as the column you created
            dataTable.Rows.Add(1, "Court 15", 0, 1630D);


            // Method 2: Create new DataRow
            var dataRow = dataTable.NewRow();
            dataRow["Id"] = 2;
            dataRow["Name"] = "Court 16";
            dataRow["Type"] = 1;
            dataRow["Price"] = 1236.50;

            // Add new row to DataTable
            dataTable.Rows.Add(dataRow);

            // After adding all rows,  AcceptChanges
            dataTable.AcceptChanges();

            //ProcessDataTable(dataTable);

            return dataTable;
        }

        public System.Data.DataTable CloneDataTable()
        {
            var dataTable = BuildDataTable();

            // Clone just the structure of DataTable, no data
            var cloneDataTable = dataTable.Clone();

            ProcessDataTable(cloneDataTable, false);

            return cloneDataTable;
        }

        public System.Data.DataTable CopyDataTable()
        {
            var dataTable = BuildDataTable();

            // Clone the structure and copy all data
            var copyDataTable = dataTable.Copy();

            ProcessDataTable(copyDataTable);

            return copyDataTable;
        }

        public System.Data.DataTable SelectCopyRowByRow()
        {
            var dataTable = new DataTable();

            // Get Courts from table
            var courts = dataTable.GetCourtsAsDataTable();

            // Clone structure into new DataTable
            var clone = courts.Clone();

            // Select Rows from DataTableObject
            var rows = courts.Select("Price < 2300", "Id");
            
            // Loop through array of rows
            foreach (var row in rows)
            {
                // NOTE: The following causes an error
                // A single row can not belong to more than one data table
                // clone.Rows.Add(row)

                // Method 1: User ItemArray() to avoid error
                // clone.Rows.Add(row.ItemArray);

                // Method 2: User ImportRow() to avoid error
                clone.ImportRow(row);
            }

            clone.AcceptChanges();

            ProcessDataTable(clone);

            return clone;
        }

        public System.Data.DataTable SelectUsingCopyToDataTable()
        {
            var dataTable = new DataTable();
            var courts = dataTable.GetCourtsAsDataTable();
            
            // Select Rows from DataTableObject and Copy to New DataTable
            var copyData = courts.Select("Price < 2300", "Id").CopyToDataTable();

            ProcessDataTable(copyData);

            return copyData;
        }

        protected virtual void ProcessDataTable(System.Data.DataTable data, bool value = true)
        {
            if (value)
            {
                foreach (DataRow row in data.Rows)
                {
                    foreach (DataColumn column in data.Columns)
                    {
                        Console.WriteLine($"{column.ColumnName}: {row[column]}");
                    }

                    Console.WriteLine();
                }

                return;
            }

            foreach (DataColumn column in data.Columns)
            {
                Console.WriteLine($"{column.ColumnName}: -");
            }

        }
    }
}
