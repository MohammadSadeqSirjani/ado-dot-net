﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection.Metadata;
using System.Text;

namespace AdoDotNet.Sql
{
    public class Builder
    {
        // Create connection string
        private const string ConnectionString =
            "Server=localhost;Database=TennisBookings;Trusted_Connection=True;Application Name=AdoDotNet";

        public void BreakApartConnectionString()
        {
            var connectionDetails = new StringBuilder(1024);

            var builder = new SqlConnectionStringBuilder(ConnectionString);

            connectionDetails.AppendLine($"Application Name: {builder.ApplicationName}");
            connectionDetails.AppendLine($"Data Source: {builder.DataSource}");
            connectionDetails.AppendLine($"Initial Catalog: {builder.InitialCatalog}");
            connectionDetails.AppendLine($"User Id: {builder.UserID}");
            connectionDetails.AppendLine($"Password: {builder.Password}");
            connectionDetails.AppendLine($"Integrated Security: {builder.IntegratedSecurity}");

            Console.WriteLine(connectionDetails.ToString());
        }

        public void CreateConnectionString()
        {
            var builder = new SqlConnectionStringBuilder()
            {
                ApplicationName = "AdoDotNet",
                ConnectTimeout = 5,
                DataSource = "localhost",
                InitialCatalog = "TennisBookings",
                IntegratedSecurity = true
            };

            Console.WriteLine(builder);
        }

        public void CreateDataModificationCommands()
        {
            const string query = "SELECT Id, Name, Type, Price FROM Courts";
            try
            {
                // Create SQL connection object
                using var connection = new SqlConnection(ConnectionString);
                // Create SQL DataAdapter object
                using var adapter = new SqlDataAdapter(query, connection);
                // Fill DataTable
                var data = new System.Data.DataTable();
                adapter.Fill(data);
                // Create a command builder
                using var commandBuilder = new SqlCommandBuilder(adapter);
                // Build INSERT Command
                // Pass true to generate parameters names matching column names
                Console.WriteLine(
                    $"INSERT =>  {commandBuilder.GetInsertCommand(true).CommandText} {Environment.NewLine}");
                // Build UPDATE Command
                Console.WriteLine(
                    $"UPDATE =>  {commandBuilder.GetUpdateCommand(true).CommandText} {Environment.NewLine}");
                // Build DELETE Command
                Console.WriteLine(
                    $"DELETE =>  {commandBuilder.GetDeleteCommand(true).CommandText} {Environment.NewLine}");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        public void InsertDataModificationCommand()
        {
            const string query = "SELECT Id, Name, Type, Price FROM Courts";
            try
            {
                // Create SQL Connection
                using var connection = new SqlConnection(ConnectionString);
                // Create SQL DataAdapter
                using var adapter = new SqlDataAdapter(query, connection);
                // Fill DataAdapter
                var data = new System.Data.DataTable();
                adapter.Fill(data);
                // Create Command Builder Object
                using var commandBuilder = new SqlCommandBuilder(adapter);
                // Build INSERT Command
                using var command = commandBuilder.GetInsertCommand(true);
                // Set generated parameters with values to insert
                command.Parameters["@Name"].Value = "Court 15";
                command.Parameters["@Type"].Value = 1;
                command.Parameters["@Price"].Value = 2450.50D;

                // Set a connection into the command object 
                command.Connection = connection;
                // Open connection
                connection.Open();
                // Execute the command
                command.ExecuteNonQuery();

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
