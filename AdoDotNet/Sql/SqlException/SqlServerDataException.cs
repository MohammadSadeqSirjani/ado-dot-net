﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace AdoDotNet.Sql.SqlException.Manager
{
    public class SqlServerDataException : Exception
    {
        public SqlServerDataException() : base() { }
        public SqlServerDataException(string message) : base(message) { }
        public SqlServerDataException(string message, Exception innerException) : base(message, innerException) { }

        public IDataParameterCollection CommandParameters { get; set; }
        public string Sql { get; set; }

        private string _connectionString = string.Empty;
        public string ConnectionString
        {
            get => HideLoginInfoForConnectionString(_connectionString);
            set => _connectionString = value;
        }
        public string Database { get; set; }
        public string WorkstationId { get; set; }

        /// <summary>
        /// Looks for UID, User Id, Pwd, Password, etc. in a connection string and replaces their 'values' with astericks.
        /// </summary>
        /// <param name="connectionString">connection string for connecting to database</param>
        /// <returns>A string with hidden user id and password values</returns>
        protected virtual string HideLoginInfoForConnectionString(string connectionString)
        {
            var builder = new SqlConnectionStringBuilder(connectionString);

            if (!string.IsNullOrEmpty(builder.UserID))
            {
                builder.UserID = "******";
            }

            if (!string.IsNullOrEmpty(builder.Password))
            {
                builder.Password = "******";
            }

            return builder.ConnectionString;
        }

        /// <summary>
        /// Gets all parameter names and values from the Command property and returns them all as a CRLF delimited string
        /// </summary>
        /// <returns>A string with all parameter names and values</returns>
        protected virtual string GetCommandParametersAsString()
        {
            var stringBuilder = new StringBuilder(1024);

            if (CommandParameters == null) return stringBuilder.ToString();
            if (CommandParameters.Count <= 0) return stringBuilder.ToString();
            stringBuilder = new StringBuilder(1024);

            foreach (IDbDataParameter param in CommandParameters)
            {
                stringBuilder.Append("  " + param.ParameterName);
                if (param.Value == null)
                    stringBuilder.AppendLine(" = null");
                else
                    stringBuilder.AppendLine(" = " + param.Value);
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Create a string with as much specific database error as possible
        /// </summary>
        /// <param name="ex">The exception</param>
        /// <returns>A string</returns>
        protected virtual string GetDatabaseSpecificError(Exception ex)
        {
            var stringBuilder = new StringBuilder(1024);

            if (!(ex is System.Data.SqlClient.SqlException))
            {
                stringBuilder.Append(ex.Message);
            }
            else
            {
                var exp = ((System.Data.SqlClient.SqlException)(ex));

                for (var index = 0; index <= exp.Errors.Count - 1; index++)
                {
                    stringBuilder.AppendLine();
                    stringBuilder.AppendLine(new string('*', 40));
                    stringBuilder.AppendLine($"**** BEGIN: SQL Server Exception # {index + 1} ****");
                    stringBuilder.AppendLine($"            Type: {exp.Errors[index].GetType().FullName}");
                    stringBuilder.AppendLine($"            Message: {exp.Errors[index].Message}");
                    stringBuilder.AppendLine($"            Source: {exp.Errors[index].Source}");
                    stringBuilder.AppendLine($"            Number: {exp.Errors[index].Number}");
                    stringBuilder.AppendLine($"            State: {exp.Errors[index].State}");
                    stringBuilder.AppendLine($"            Class: {exp.Errors[index].Class}");
                    stringBuilder.AppendLine($"            Server: {exp.Errors[index].Server}");
                    stringBuilder.AppendLine($"            Procedure: {exp.Errors[index].Procedure}");
                    stringBuilder.AppendLine($"            LineNumber: {exp.Errors[index].LineNumber}");
                    stringBuilder.AppendLine($"**** END: SQL Server Exception #{index + 1} ****");
                    stringBuilder.AppendLine(new string('*', 40));
                }
            }

            return stringBuilder.ToString();
        }
    }
}
