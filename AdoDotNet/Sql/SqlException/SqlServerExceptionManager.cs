﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Text;

namespace AdoDotNet.Sql.SqlException.Manager
{
    public class SqlServerExceptionManager
    {
        private static SqlServerExceptionManager _instance;

        // Get/Set Last Exception Object Created
        public static SqlServerExceptionManager Instance
        {
            get { return _instance ??= new SqlServerExceptionManager(); }
            set => _instance = value;
        }

        public Exception LastException { get; set; }

        public virtual void Publish(Exception exception)
        {
            LastException = exception;

            // TODO: Implement an exception publisher here
            System.Diagnostics.Debug.WriteLine(exception);
        }


        public virtual void Publish(Exception exception, SqlCommand command)
        {
            Publish(exception, command, null);
        }

        public virtual void Publish(Exception exception, SqlCommand command, string message)
        {
            LastException = exception;

            if (command == null) return;
            LastException = CreateDbException(exception, command, null);

            // TODO: Implement an exception publisher here
            System.Diagnostics.Debug.WriteLine(LastException);
        }

        protected virtual SqlServerDataException CreateDbException(Exception exception, SqlCommand command,
            string message)
        {
            message = string.IsNullOrEmpty(message) ? string.Empty : $"{message} - ";

            var dataException = new SqlServerDataException($"{message} {exception.Message}", exception)
            {
                ConnectionString = command.Connection.ConnectionString,
                Database = command.Connection.Database,
                Sql = command.CommandText,
                CommandParameters = command.Parameters,
                WorkstationId = Environment.MachineName
            };

            return dataException;
        }
    }
}
