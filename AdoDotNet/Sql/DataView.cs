﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AdoDotNet.Enums;

namespace AdoDotNet.Sql
{
    public class DataView
    {
        // Create connection string
        private const string ConnectionString =
            "Server=localhost;Database=TennisBookings;Trusted_Connection=True;";

        public System.Data.DataView GetCourtsSortedByPriceDescending()
        {
            var table = new DataTable();
            //Get Courts as DataTable
            var courts = table.GetCourtsAsDataTable();

            if (courts == null) return new System.Data.DataView();

            // Set DataView
            var view = courts.DefaultView;

            // Sort the data
            view.Sort = "Price DESC";

            // Rows affected
            var rowsAffected = view.Count;

            Console.WriteLine($"Rows affected: {rowsAffected}");

            return view;
        }

        public System.Data.DataView GetCourtsFilteredPrice(decimal price)
        {
            var table = new Sql.DataTable();
            // Get Courts as DataTable
            var courts = table.GetCourtsAsDataTable();

            if (courts == null) return new System.Data.DataView();

            // Set DataView
            var view = courts.DefaultView;

            // Filter the data
            // Can filter using And, Or, True, False, Is, Like, etc.
            view.RowFilter = $"Price < {price}";

            // Rows affected
            var rowAffected = view.Count;

            Console.WriteLine($"Rows affected: {rowAffected}");

            return view;
        }

        public System.Data.DataView GetCourtsFilteredByLinq(decimal price)
        {
            var table = new DataTable();
            var courts = table.GetCourtsAsDataTable();

            var view = courts
                .AsEnumerable()
                .Where(court => court.Field<decimal>("Price") < price)
                .OrderByDescending(court => court.Field<decimal>("Price"))
                .AsDataView();

            var rowsAffected = view.Count;

            Console.WriteLine($"Rows affected: {rowsAffected}");

            return view;
        }

        public System.Data.DataTable DataViewToDataTable()
        {
            var table = new DataTable();
            var courts = table.GetCourtsAsDataTable();

            var view = courts
                .AsEnumerable()
                .Where(court => court.Field<decimal>("Price") < 2500)
                .OrderByDescending(court => court.Field<decimal>("Price"))
                .AsDataView();

            var rowsAffected = view.Count;

            var filteredCourts = view.ToTable();

            Console.WriteLine($"Rows affected: {rowsAffected}");

            foreach (DataRow row in filteredCourts.Rows)
            {
                Console.WriteLine($"Id = {row.Field<int>("Id")}");
                var type = row.Field<int>("Type") == 0 ? CourtType.Indoor : CourtType.Outdoor;
                Console.WriteLine($"Court Type = {type}");
                Console.WriteLine($"Name = {row.Field<string>("Name")}");
                Console.WriteLine($"Price = {row.Field<decimal>("Price")}");
                Console.WriteLine();
            }

            return filteredCourts;
        }
    }
}
