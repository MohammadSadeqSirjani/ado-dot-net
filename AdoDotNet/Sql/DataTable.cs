﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using AdoDotNet.Enums;
using AdoDotNet.Models;

namespace AdoDotNet.Sql
{
    public class DataTable
    {
        // Create connection string
        private const string ConnectionString =
            "Server=localhost;Database=TennisBookings;Trusted_Connection=True;";

        public System.Data.DataTable GetCourtsAsDataTable()
        {
            // Create SQL statement to submit
            const string query = "SELECT * FROM Courts";

            // Create SQL Connection 
            using var connection = new SqlConnection(ConnectionString);
            // Open SQL Connection
            connection.Open();
            // Create SQL Command
            using var command = new SqlCommand(query, connection);
            // Create SQL DataAdapter
            using var adapter = new SqlDataAdapter(command);

            //Create new DataTable object for filling
            var dataTable = new System.Data.DataTable();

            // Fill DataTable using DataAdapter
            adapter.Fill(dataTable);

            ProcessRowsAndColumns(dataTable);

            return dataTable;
        }

        public List<Courts> GetCourtsAsGenericList()
        {
            // Create a SQL query to submit
            const string query = "SELECT * FROM Courts";

            // Create a SQL connection
            using var connection = new SqlConnection(ConnectionString);
            // Open SQL connection
            connection.Open();
            // Create SQL command
            using var command = new SqlCommand(query, connection)
            {
                // Set the command type as text
                CommandType = CommandType.Text
            };

            // Create SQL data adapter
            using var adapter = new SqlDataAdapter(command);
            // Create data table
            var table = new System.Data.DataTable();

            // Fill data adapter into data table
            adapter.Fill(table);
            if (table.Rows.Count <= 0) return null;
            // Iterate item in data table 
            var courts = table.AsEnumerable().Select(row => new Courts()
            {
                Id = row.Field<int>("Id"),
                CourtType = row.Field<int>("Type"),
                Name = row.Field<string>("Name"),
                Price = row.Field<decimal>("Price")
            }).ToList();


            foreach (var court in courts)
            {
                Console.WriteLine($"Id: {court.Id}");
                var type = court.CourtType == 0 ? CourtType.Indoor : CourtType.Outdoor;
                Console.WriteLine($"Court Type: {type}");
                Console.WriteLine($"Name: {court.Name}");
                Console.WriteLine($"Price: {court.Price}");
                Console.WriteLine();
            }

            return courts;
        }

        public void GetMultipleResult()
        {
            var dataSet = new DataSet();

            const string query = "SELECT * FROM Courts; SELECT * FROM CourtMaintenance;";

            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            using var command = new SqlCommand(query, connection);
            using var adapter = new SqlDataAdapter(command);

            adapter.Fill(dataSet);

            if (dataSet.Tables.Count <= 0) return;
            var courts = dataSet.Tables[0];
            var courtMaintenance = dataSet.Tables[1];

            foreach (DataRow row in courts.Rows)
            {
                Console.WriteLine($"Id: {row.Field<int>("Id")}");
                var type = row.Field<int>("Type") == 0 ? CourtType.Indoor : CourtType.Outdoor;
                Console.WriteLine($"Court Type: {type}");
                Console.WriteLine($"Name: {row.Field<string>("Name")}");
                Console.WriteLine($"Price: {row.Field<decimal>("Price")}");
                Console.WriteLine();
            }

            Console.WriteLine(new string('*',50));

            foreach (DataRow row in courtMaintenance.Rows)
            {
                Console.WriteLine($"Id: {row.Field<int>("Id")}");
                Console.WriteLine($"Work Title: {row.Field<string>("WorkTitle")}");
                Console.WriteLine($"Court Is Closed: {row.Field<bool>("CourtIsClosed")}");
                Console.WriteLine($"Start Date: {row.Field<DateTime>("StartDate")}");
                Console.WriteLine($"End Date: {row.Field<DateTime>("EndDate")}");
                Console.WriteLine($"Court Id: {row.Field<int>("CourtId")}");
                Console.WriteLine();
            }
        }

        public void ProcessRowsAndColumns(System.Data.DataTable table)
        {
            var stringBuilder = new StringBuilder();
            var index = 1;

            foreach (DataRow row in table.Rows)
            {
                stringBuilder.AppendLine($"*** ROW: {index} ***");
                foreach (DataColumn column in table.Columns)
                {
                    stringBuilder.AppendLine($"{column.ColumnName}: {row[column]}");
                }

                stringBuilder.AppendLine();
                index++;
            }

            //Console.WriteLine(stringBuilder);
        }
    }
}
