﻿using System;
using System.Data;
using System.Data.SqlClient;
using AdoDotNet.Enums;

namespace AdoDotNet.Sql
{
    public class Command
    {
        // Create connection string
        private const string ConnectionString =
            "Server=localhost;Database=TennisBookings;Trusted_Connection=True;";

        public void GetCourtsCountScalar()
        {
            // Create SQL statement to submit
            const string query = "SELECT COUNT(*) FROM Courts";

            try
            {
                // Create connection
                using var connection = new SqlConnection(ConnectionString);
                // Open the connection
                connection.Open();
                // Create command object
                using var command = new SqlCommand(query, connection);

                // Execute command
                var rowsAffected = command.ExecuteScalar() is int ? (int)command.ExecuteScalar() : 0;

                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        public void GetCourtsCountScalarUsingParameter()
        {
            // Create SQL statement to submit
            const string query = "SELECT COUNT(*) FROM Courts WHERE Type = @Type";

            // Create a connection
            using var connection = new SqlConnection(ConnectionString);
            // Open the connection
            connection.Open();
            // Create command object
            using var command = new SqlCommand(query, connection);

            // Create a parameter
            command.Parameters.Add(new SqlParameter("@Type", 1));

            // Execute command
            var rowsAffected = command.ExecuteScalar() is int ? (int)command.ExecuteScalar() : 0;

            Console.WriteLine($"Row affected: {rowsAffected}");
        }

        public void InsertCourt()
        {
            // Create SQL statement to submit
            const string query =
                "INSERT INTO Courts (Type, Name) VALUES (0, 'Court 6'), (0, 'Court 7'), (1, 'Court 8')";

            try
            {
                // Create SQL connection object in using block for automatic closing and disposing
                using var connection = new SqlConnection(ConnectionString);
                // Open the connection
                connection.Open();
                // Create command object in using block for automatic closing and disposing
                using var command = new SqlCommand(query, connection)
                {
                    // Set command type
                    CommandType = CommandType.Text
                };

                // Execute the INSERT statement
                var rowsAffected = command.ExecuteNonQuery();

                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        public void InsertCourtsUsingParameters(CourtType type, string name)
        {
            // Create SQL statement to submit
            const string query = "INSERT INTO Courts (Type, Name) VALUES (@Type, @Name)";

            try
            {
                // Create the connection object in using block for automatic closing and disposing
                using var connection = new SqlConnection(ConnectionString);
                // Open the connection
                connection.Open();
                // Create Command object in using block for automatic closing and disposing
                using var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text
                };

                // Create inputs parameters
                command.Parameters.Add(new SqlParameter("@Type", type));
                command.Parameters.Add(new SqlParameter("@Name", name));

                // Execute the INSERT statement
                var rowsAffected = command.ExecuteNonQuery();

                Console.WriteLine($"Rows affected: {rowsAffected}");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        public void InsertCourtOutputParameter(CourtType type, string name)
        {
            // Create SQL statement to submit
            const string query = "sp_court_insert";
            var id = 0;

            // Create SQL connection object in using block for automatic closing and disposing 
            using var connection = new SqlConnection(ConnectionString);
            // Open the connection
            connection.Open();
            // Create command object in using block for automatic disposal
            using var command = new SqlCommand(query, connection)
            {
                // Set Command Type to Stored Procedure
                CommandType = CommandType.StoredProcedure
            };

            // Create input parameters
            command.Parameters.Add(new SqlParameter("@Type", type));
            command.Parameters.Add(new SqlParameter("@Name", name));

            // Create Output parameter
            command.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@Id",
                Value = id,
                DbType = DbType.Int32,
                IsNullable = false,
                Direction = ParameterDirection.Output
            });

            // Execute the INSERT statement
            var rowsAffected = command.ExecuteNonQuery();

            // Get output parameter
            id = command.Parameters["@Id"].Value as int? ?? 0;

            Console.WriteLine($"Rows affected: {rowsAffected}");
            Console.WriteLine($"Id = {id}");
        }

        public void TransactionProcessing(CourtType type, string name)
        {
            // Create SQL statement to submit
            var query = "sp_court_insert";
            var id = 0;

            try
            {
                // Create SQL connection in using block for automatic closing and disposing
                using var connection = new SqlConnection(ConnectionString);
                // Open the connection
                connection.Open();
                // Start a local transaction
                using var transaction = connection.BeginTransaction();
                try
                {
                    // Create command object in using block for automatic disposal
                    using var command = new SqlCommand(query, connection)
                    {
                        CommandType = CommandType.StoredProcedure,
                        // Make a command object as part of the transaction
                        Transaction = transaction
                    };

                    // Create inputs parameters
                    command.Parameters.Add(new SqlParameter("@Type", type));
                    command.Parameters.Add(new SqlParameter("@Name", name));

                    // Create OUTPUT parameter
                    command.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@Id",
                        Value = id,
                        DbType = DbType.Int32,
                        IsNullable = false,
                        Direction = ParameterDirection.Output
                    });

                    // Execute INSET statement
                    var rowsAffected = command.ExecuteNonQuery();

                    // Get output parameter
                    id = command.Parameters["@Id"].Value as int? ?? 0;

                    Console.WriteLine($"Rows affected: {rowsAffected}");
                    Console.WriteLine($"Id = {id}");

                    // Second statement to execute
                    query =
                        "INSERT INTO CourtMaintenance (WorkTitle, CourtIsClosed, StartDate, EndDate, CourtId) " +
                        "VALUES (@WorkTitle, @CourtIsClosed, @StartDate, @EndDate, @CourtId)";

                    // Reset the command text 
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    // Create previous parameter
                    command.Parameters.Clear();
                    // Create input parameters
                    command.Parameters.Add(new SqlParameter("@WorkTitle", "Replace Bat"));
                    command.Parameters.Add(new SqlParameter("@CourtIsClosed", false));
                    command.Parameters.Add(new SqlParameter("@StartDate", DateTime.Today));
                    command.Parameters.Add(new SqlParameter("@EndDate", DateTime.Today.AddDays(2).Date));
                    command.Parameters.Add(new SqlParameter("@CourtId", id));

                    // Execute the INSERT statement
                    rowsAffected = command.ExecuteNonQuery();

                    Console.WriteLine($"Rows affected: {rowsAffected}");

                    // Finish transaction
                    transaction.Commit();
                }
                catch (Exception exception) // Catch block for transaction
                {
                    // Rollback the transaction 
                    transaction.Rollback();
                    Console.WriteLine(exception.Message);
                }
            }
            catch (Exception exception)  // Catch block for connection opening
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
