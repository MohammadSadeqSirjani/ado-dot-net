﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using AdoDotNet.Enums;
using AdoDotNet.Models;
using AdoDotNet.Sql.Utilities;

namespace AdoDotNet.Sql
{
    public class DataReader
    {
        // Create connection string
        private const string ConnectionString =
            "Server=localhost;Database=TennisBookings;Trusted_Connection=True;";

        public void GetCourtsAsDataReader()
        {
            // Create SQL statement to submit
            const string query = "SELECT * FROM Courts";
            var stringBuilder = new StringBuilder();

            // Create a connection object in using block for automatic closing and disposing
            using var connection = new SqlConnection(ConnectionString);
            // Open a connection
            connection.Open();
            // Create a command object in using block for automatic disposal
            using var command = new SqlCommand(query, connection);
            using var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
            stringBuilder.AppendLine("Court Detail:");
            stringBuilder.AppendLine("-----------------------");
            while (dataReader.Read())
            {
                stringBuilder.AppendLine($"Id: {dataReader.GetOrdinal("Id")}");
                var type = dataReader[$"Type"].ToString().Equals("0") ? CourtType.Indoor : CourtType.Outdoor;
                stringBuilder.AppendLine($"Court Type: {type}");
                stringBuilder.AppendLine($"Court Name: {dataReader["Name"]}");
                stringBuilder.AppendLine("***********************");
            }

            Console.WriteLine(stringBuilder.ToString());
        }

        public void GetCourtMaintenanceAsGenericList()
        {
            var courtsMaintenance = new List<CourtMaintenance>();
            const string query = "SELECT * FROM CourtMaintenance";

            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            using var command = new SqlCommand(query, connection);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            while (reader.Read())
            {
                courtsMaintenance.Add(new CourtMaintenance()
                {
                    Id = Convert.ToInt32(reader["id"]),
                    WorkTitle = reader.IsDBNull(reader.GetOrdinal("WorkTitle"))
                        ? "\"\""
                        : reader.GetString(reader.GetOrdinal("WorkTitle")),
                    CourtIsClosed = reader.GetBoolean(reader.GetOrdinal("CourtIsClosed")),
                    StartDate = reader.GetDateTime(reader.GetOrdinal("StartDate")),
                    EndDate = reader.GetDateTime(reader.GetOrdinal("EndDate")),
                    CourtId = reader.GetOrdinal("CourtId")
                });
            }

            foreach (var courtMaintenance in courtsMaintenance)
            {
                Console.WriteLine($"Id = {courtMaintenance.Id}");
                Console.WriteLine($"WorkTitle = {courtMaintenance.WorkTitle}");
                Console.WriteLine($"CourtIsClosed = {courtMaintenance.CourtIsClosed}");
                Console.WriteLine($"StartDate = {courtMaintenance.StartDate}");
                Console.WriteLine($"EndDate = {courtMaintenance.EndDate}");
                Console.WriteLine($"CourtId = {courtMaintenance.CourtId}");
                Console.WriteLine();
            }
        }

        public void GetCourtMaintenanceUsingFieldValue()
        {
            const string query = "SELECT * FROM CourtMaintenance";
            var courtsMaintenance = new List<CourtMaintenance>();

            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            using var command = new SqlCommand(query, connection);

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            while (reader.Read())
            {
                courtsMaintenance.Add(new CourtMaintenance()
                {
                    Id = reader.GetFieldValue<int>(reader.GetOrdinal("id")),
                    // NOTE: GetFieldValue() does not work on nullable fields
                    WorkTitle = reader.IsDBNull(reader.GetOrdinal("WorkTitle"))
                        ? "\"\""
                        : reader.GetFieldValue<string>(reader.GetOrdinal("WorkTitle")),
                    CourtIsClosed = reader.GetFieldValue<bool>(reader.GetOrdinal("CourtIsClosed")),
                    StartDate = reader.GetFieldValue<DateTime>(reader.GetOrdinal("StartDate")),
                    EndDate = reader.GetFieldValue<DateTime>(reader.GetOrdinal("EndDate")),
                    CourtId = reader.GetFieldValue<int>(reader.GetOrdinal("CourtId"))
                });
            }

            foreach (var courtMaintenance in courtsMaintenance)
            {
                Console.WriteLine($"Id = {courtMaintenance.Id}");
                Console.WriteLine($"WorkTitle = {courtMaintenance.WorkTitle}");
                Console.WriteLine($"CourtIsClosed = {courtMaintenance.CourtIsClosed}");
                Console.WriteLine($"StartDate = {courtMaintenance.StartDate}");
                Console.WriteLine($"EndDate = {courtMaintenance.EndDate}");
                Console.WriteLine($"CourtId = {courtMaintenance.CourtId}");
                Console.WriteLine();
            }
        }

        public void GetProductsUsingExtensionMethods()
        {
            const string query = "SELECT * FROM CourtMaintenance";
            var courtsMaintenance = new List<CourtMaintenance>();

            using var connection = new SqlConnection(ConnectionString);
            connection.Open();
            using var command = new SqlCommand(query, connection);

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            while (reader.Read())
            {
                courtsMaintenance.Add(new CourtMaintenance()
                {
                    Id = reader.GetFieldValue<int>("id"),
                    WorkTitle = reader.GetFieldValue<string>("WorkTitle"),
                    CourtIsClosed = reader.GetFieldValue<bool>("CourtIsClosed"),
                    StartDate = reader.GetFieldValue<DateTime>("StartDate"),
                    EndDate = reader.GetFieldValue<DateTime>("EndDate"),
                    CourtId = reader.GetFieldValue<int>("CourtId")
                });
            }

            foreach (var courtMaintenance in courtsMaintenance)
            {
                Console.WriteLine($"Id = {courtMaintenance.Id}");
                Console.WriteLine($"WorkTitle = {courtMaintenance.WorkTitle}");
                Console.WriteLine($"CourtIsClosed = {courtMaintenance.CourtIsClosed}");
                Console.WriteLine($"StartDate = {courtMaintenance.StartDate}");
                Console.WriteLine($"EndDate = {courtMaintenance.EndDate}");
                Console.WriteLine($"CourtId = {courtMaintenance.CourtId}");
                Console.WriteLine();
            }
        }

        public void GetMultipleResultSets()
        {
            // Create SQL statement to submit
            var query = "SELECT * FROM Courts";
            query += ";SELECT * FROM CourtMaintenance";
            var courtsMaintenance = new List<CourtMaintenance>();
            var courts = new List<Courts>();

            // Create a connection
            using var connection = new SqlConnection(ConnectionString);
            // Open a connection
            connection.Open();
            // Create a command
            using var command = new SqlCommand(query, connection);
            // Execute command to get data reader
            using var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                courts.Add(new Courts()
                {
                    Id = reader.GetFieldValue<int>("id"),
                    CourtType = reader.GetFieldValue<int>("Type"),
                    Name = reader.GetFieldValue<string>("Name")
                });
            }

            foreach (var court in courts)
            {
                Console.WriteLine($"Id: {court.Id}");
                var type = court.CourtType == 0 ? CourtType.Indoor : CourtType.Outdoor;
                Console.WriteLine($"Court Type: {type}");
                Console.WriteLine($"Name: {court.Name}");
                Console.WriteLine();
            }

            Console.WriteLine("------------------");

            // Move to next result set
            reader.NextResult();

            // Read next result set
            while (reader.Read())
            {
                courtsMaintenance.Add(new CourtMaintenance()
                {
                    Id = reader.GetFieldValue<int>("id"),
                    WorkTitle = reader.GetFieldValue<string>("WorkTitle"),
                    CourtIsClosed = reader.GetFieldValue<bool>("CourtIsClosed"),
                    StartDate = reader.GetFieldValue<DateTime>("StartDate"),
                    EndDate = reader.GetFieldValue<DateTime>("EndDate"),
                    CourtId = reader.GetFieldValue<int>("CourtId")
                });
            }

            foreach (var courtMaintenance in courtsMaintenance)
            {
                Console.WriteLine($"Id = {courtMaintenance.Id}");
                Console.WriteLine($"WorkTitle = {courtMaintenance.WorkTitle}");
                Console.WriteLine($"CourtIsClosed = {courtMaintenance.CourtIsClosed}");
                Console.WriteLine($"StartDate = {courtMaintenance.StartDate}");
                Console.WriteLine($"EndDate = {courtMaintenance.EndDate}");
                Console.WriteLine($"CourtId = {courtMaintenance.CourtId}");
                Console.WriteLine();
            }
        }
    }
}
