﻿using System;
using System.Collections.Generic;
using Npgsql;
using System.Text;
using Common.DataLayer;
using System.Data;
using System.Data.Common;

namespace AdoDotNet.Common.Common.DataLayer.PostgreSql
{
    public class PostgreSqlDataManagerBase : DataManagerBase
    {
        #region Constructor
        public PostgreSqlDataManagerBase(string nameOrConnectionString) : base(nameOrConnectionString) { }
        #endregion

        #region Properties
        public int ReturnValue { get; set; }
        #endregion

        #region Initialize Method
        /// <summary>
        /// This method is called by the constructor
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();

            Command = new NpgsqlCommand();
            ParameterToken = "@";
        }
        #endregion

        #region Reset Methods
        public override void Reset(CommandType type)
        {
            base.Reset(type);

            Command ??= new NpgsqlCommand
            {
                CommandType = type
            };

            ReturnValue = 0;
        }
        #endregion

        #region CreateConnection Method
        public override IDbConnection CreateConnection(string connectString) => new NpgsqlConnection(connectString);
        #endregion

        #region CreateCommand Method
        public override IDbCommand CreateCommand() => new NpgsqlCommand();

        #endregion

        #region CreateDataAdapter Method
        public override DbDataAdapter CreateDataAdapter(IDbCommand command) => new NpgsqlDataAdapter((NpgsqlCommand)command);
        #endregion

        #region CreateParameter Methods
        public override IDbDataParameter CreateParameter(string name, object value, bool isNullable)
        {
            // Ensure parameter name contains token
            name = name.Contains(ParameterToken) ? name : ParameterToken + name;
            // Add parameter
            return new NpgsqlParameter { ParameterName = name, Value = value, IsNullable = isNullable };
        }

        public override IDbDataParameter CreateParameter(string name, object value, bool isNullable, System.Data.DbType type, System.Data.ParameterDirection direction = System.Data.ParameterDirection.Input)
        {
            // Ensure parameter name contains token
            name = name.Contains(ParameterToken) ? name : ParameterToken + name;
            // Add parameter
            return new NpgsqlParameter { ParameterName = name, Value = value, IsNullable = isNullable, DbType = type, Direction = direction };
        }

        public override IDbDataParameter CreateParameter(string name, object value, bool isNullable, System.Data.DbType type, int size, System.Data.ParameterDirection direction = System.Data.ParameterDirection.Input)
        {
            // Ensure parameter name contains token
            name = name.Contains(ParameterToken) ? name : ParameterToken + name;
            // Add parameter
            return new NpgsqlParameter { ParameterName = name, Value = value, IsNullable = isNullable, DbType = type, Direction = direction, Size = size };
        }
        #endregion

        #region AddStandardParameters Method
        public override void AddStandardParameters()
        {
            if (Command.CommandType == CommandType.StoredProcedure)
                AddParameter("RETURN_VALUE", 0, false, DbType.Int32, ParameterDirection.ReturnValue);
        }
        #endregion

        #region GetOutputParameters Method
        public override void GetStandardOutputParameters()
        {
            if (Command.CommandType == CommandType.StoredProcedure)
                ReturnValue = GetParameterValue<int>("RETURN_VALUE", default(int));
        }
        #endregion

        #region GetParameterValue Method
        public override T GetParameterValue<T>(string name, object defaultValue)
        {
            T result;

            var value = ((NpgsqlParameter)GetParameter(name)).Value.ToString();
            if (string.IsNullOrEmpty(value))
            {
                result = (T)defaultValue;
            }
            else
            {
                result = (T)Convert.ChangeType(value, typeof(T));
            }

            return result;
        }
        #endregion

        #region ThrowDbException Method
        public override void ThrowDbException(Exception exception, IDbCommand command, string exceptionMessage = "")
        {
            DataException dataException;
            exceptionMessage = string.IsNullOrEmpty(exceptionMessage) ? string.Empty : exceptionMessage + " - ";

            if (exception is NpgsqlException)
            {
                dataException = new PostgreSqlDataException($"{exceptionMessage}{exception.Message}", exception)
                {
                    ConnectionString = command.Connection.ConnectionString,
                    ConnectionStringInAppSetting = ConnectStringName,
                    DataBase = command.Connection.Database,
                    Sql = SqlCommand,
                    CommandParameters = command.Parameters,
                    WorkStationId = Environment.MachineName
                };
            }
            else
            {
                dataException = new DataException($"{exceptionMessage}{exception.Message}", exception)
                {
                    ConnectionString = command.Connection.ConnectionString,
                    ConnectionStringInAppSetting = ConnectStringName,
                    DataBase = command.Connection.Database,
                    Sql = SqlCommand,
                    CommandParameters = command.Parameters,
                    WorkStationId = Environment.MachineName
                };
            }

            // Set the last exception
            LastException = dataException;

            // Throw the exception
            throw dataException;
        }
        #endregion
    }
}
