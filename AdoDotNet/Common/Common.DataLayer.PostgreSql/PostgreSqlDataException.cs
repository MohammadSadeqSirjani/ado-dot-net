﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Npgsql;

namespace AdoDotNet.Common.Common.DataLayer.PostgreSql
{
    public class PostgreSqlDataException : DataException
    {
        #region Constructors
        public PostgreSqlDataException() : base() { }

        public PostgreSqlDataException(string message) : base(message) { }

        public PostgreSqlDataException(string message, Exception innerException) : base(message, innerException) { }
        #endregion

        #region GetDatabaseSpecificError Method

        /// <summary>
        /// Create a string with as much specific database error as possible
        /// </summary>
        /// <param name="exception">The exception</param>
        /// <returns>A string</returns>
        protected override string GetDataBaseSpecificError(Exception exception)
        {
            var exceptionBuilder = new StringBuilder(1024);

            if (exception is NpgsqlException npsqlException)
            {
                var innerException = npsqlException;

                exceptionBuilder.AppendLine();
                exceptionBuilder.AppendLine(new string('*', 40));
                exceptionBuilder.AppendLine("**** BEGIN: PostgreSql Exception #1 ****");
                exceptionBuilder.AppendLine($"     Type: {innerException.GetType().FullName}");
                exceptionBuilder.AppendLine($"     Message: {innerException.Message}");
                exceptionBuilder.AppendLine($"     Source: {innerException.Source}");
                exceptionBuilder.AppendLine($"     TargetSite: {innerException.TargetSite}");
                exceptionBuilder.AppendLine($"     StackTrace: {innerException.StackTrace}");
                exceptionBuilder.AppendLine($"     HelpLink: {innerException.HelpLink}");
                exceptionBuilder.AppendLine("**** END: PostgreSql Exception #1 ****");
                exceptionBuilder.AppendLine(new string('*', 40));

            }
            else
            {
                exceptionBuilder.Append(exception.Message);
            }

            return exceptionBuilder.ToString();
        }
        #endregion

        #region IsDatabaseSpecificError Method
        protected override bool IsDataBaseSpecificError(Exception exception) => exception is NpgsqlException;
        #endregion
    }
}
