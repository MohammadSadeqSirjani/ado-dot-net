﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using AdoDotNet.Common.Common.Library.BaseClasses;

namespace AdoDotNet.Common.Common.DataLayer.PostgreSql
{
    public class PostgreSqlEntityBase : EntityBase
    {
        [NotMapped]
        public int ReturnValue { get; set; }
    }
}
