﻿using System;
using System.Collections.Generic;
using System.Text;
using AdoDotNet.Common.Common.Library.Validation;

namespace AdoDotNet.Common.Common.Library.Exception
{
    public sealed class ValidationException : System.Exception
    {
        public List<ValidationMessage> ValidationMessages { get; set; }

        public ValidationException() : base()
        {
            Init();
        }

        public ValidationException(string message) : base(message)
        {
            Init();
        }

        public ValidationException(string message, System.Exception exception) : base(message, exception)
        {
            Init();
        }

        public ValidationException(List<ValidationMessage> messages) : base()
        {
            Init();
            ValidationMessages = messages;
        }

        private void Init()
        {
            ValidationMessages = new List<ValidationMessage>();
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder(1024);

            foreach (var item in ValidationMessages)
            {
                stringBuilder.AppendLine($"Property Name: {item.PropertyName} - Message {item.Message}");
            }

            return stringBuilder.ToString();
        }
    }
}
