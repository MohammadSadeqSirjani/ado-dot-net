﻿using System;
using System.Collections.Generic;
using System.Text;
using AdoDotNet.Common.Common.Library.BaseClasses;

namespace AdoDotNet.Common.Common.Library.Exception
{
    public class ExceptionManger : CommonBase
    {
        private static ExceptionManger _instance;

        public static ExceptionManger Instance
        {
            get => _instance ?? new ExceptionManger();

            set => _instance = value;
        }

        public virtual void Publish(System.Exception exception)
        {
            // TODO: Implement an exception publisher here
            System.Diagnostics.Debug.WriteLine(exception.ToString());
        }
    }
}
