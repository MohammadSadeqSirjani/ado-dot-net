﻿using System;
using System.Collections.Generic;
using System.Text;
using AdoDotNet.Common.Common.Library.BaseClasses;
using Microsoft.VisualBasic.CompilerServices;

namespace AdoDotNet.Common.Common.Library.Validation
{
    public class ValidationMessage : CommonBase
    {
        private string _propertyName;

        private string _name;

        public string PropertyName
        {
            get => _propertyName;
            set
            {
                _propertyName = value;
                OnPropertyChanged(_propertyName);
            }
        }

        public string Message
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(_name);
            }
        }
    }
}
