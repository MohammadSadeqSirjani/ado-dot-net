﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using AdoDotNet.Common.Common.Library.Exception;
using AdoDotNet.Common.Common.Library.Validation;

namespace AdoDotNet.Common.Common.Library.BaseClasses
{
    public class Base : CommonBase
    {
        private ObservableCollection<ValidationMessage> _validationMessages =
            new ObservableCollection<ValidationMessage>();

        private bool _isValidationVisible;

        private System.Exception _lastException;

        private int _rowAffected;

        private string _lastExceptionMessage = string.Empty;

        public ObservableCollection<ValidationMessage> ValidationMessages
        {
            get => _validationMessages;
            set
            {
                _validationMessages = value;
                OnPropertyChanged(nameof(_validationMessages));
            }
        }

        public bool IsValidationVisible
        {
            get => _isValidationVisible;
            set
            {
                _isValidationVisible = value;
                OnPropertyChanged(nameof(_isValidationVisible));
            }
        }

        public System.Exception LastException
        {
            get => _lastException;
            set
            {
                _lastException = value;
                OnPropertyChanged(nameof(_lastException));
            }
        }

        public int RowAffected
        {
            get => _rowAffected;
            set
            {
                _rowAffected = value;
                OnPropertyChanged(nameof(_rowAffected));
            }
        }

        public string LastExceptionMessage
        {
            get => _lastExceptionMessage;
            set
            {
                _lastExceptionMessage = value;
                OnPropertyChanged(nameof(_lastExceptionMessage));
            }
        }

        protected virtual void Init()
        {
            ValidationMessages.Clear();
            IsValidationVisible = false;
        }

        protected virtual void DisplayStatusMessage(string message = "")
        {
        }

        public virtual bool Validate() => true;

        public virtual void ValidationFailed(ValidationException exception)
        {
            ValidationMessages = new ObservableCollection<ValidationMessage>(exception.ValidationMessages);
            IsValidationVisible = true;
        }

        public void PublishException(System.Exception exception)
        {
            LastException = exception;
            LastExceptionMessage = exception.Message;
            ExceptionManger.Instance.Publish(exception);
        }

        public void Close(bool wasCancelled = true)
        {
        }
    }
}
