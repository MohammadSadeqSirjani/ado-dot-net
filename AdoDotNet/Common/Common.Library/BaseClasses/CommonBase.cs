﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using AdoDotNet.Annotations;

namespace AdoDotNet.Common.Common.Library.BaseClasses
{
    public class CommonBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Clone<T>(T original, T cloneTo)
        {
            if (original == null || cloneTo == null) return;
            // Use reflection so the OnPropertyChanged event is fired each property
            foreach (var prop in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var value = prop.GetValue(original, null);
                prop.SetValue(cloneTo, value, null);
            }
        }
    }
}
