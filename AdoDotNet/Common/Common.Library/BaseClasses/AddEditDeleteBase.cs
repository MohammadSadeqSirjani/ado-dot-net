﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdoDotNet.Common.Common.Library.BaseClasses
{
    public class AddEditDeleteBase : Base
    {
        private bool _isListEnabled = true;

        private bool _isDetailEnabled = true;

        private bool _isAddMode = true;

        public bool IsListEnabled
        {
            get => _isListEnabled;
            set
            {
                _isListEnabled = value;
                OnPropertyChanged(nameof(_isListEnabled));
            }
        }

        public bool IsDetailEnabled
        {
            get => _isDetailEnabled;
            set
            {
                _isDetailEnabled = value;
                OnPropertyChanged(nameof(_isDetailEnabled));
            }
        }

        public bool IsAddMode
        {
            get => _isAddMode;
            set
            {
                _isAddMode = value;
                OnPropertyChanged(nameof(_isAddMode));
            }
        }

        public virtual void BeginEdit(bool isAddMode = true)
        {
            IsAddMode = isAddMode;
            IsListEnabled = false;
            IsDetailEnabled = true;
        }

        public virtual void CancelEdit()
        {
            base.Init();

            IsDetailEnabled = false;
            IsListEnabled = true;
            IsAddMode = false;
        }

        public virtual bool Save() => true;

        public virtual bool Delete() => true;
    }
}
