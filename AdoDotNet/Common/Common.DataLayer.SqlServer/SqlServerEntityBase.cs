﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using AdoDotNet.Common.Common.Library.BaseClasses;

namespace AdoDotNet.Common.Common.DataLayer.SqlServer
{
    public class SqlServerEntityBase : EntityBase
    {
        [NotMapped]
        public int ReturnValue { get; set; }
    }
}
