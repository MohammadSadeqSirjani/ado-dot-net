﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace AdoDotNet.Common.Common.DataLayer.SqlServer
{
    public class SqlServerDataException : DataException
    {
        #region Constructors
        public SqlServerDataException() : base() { }

        public SqlServerDataException(string message) : base(message) { }

        public SqlServerDataException(string message, Exception innerException) : base(message, innerException) { }
        #endregion

        #region GetDatabaseSpecificError Method

        /// <summary>
        /// Create a string with as much specific database error as possible
        /// </summary>
        /// <param name="exception">The exception</param>
        /// <returns>A string</returns>
        protected override string GetDataBaseSpecificError(Exception exception)
        {
            var exceptionBuilder = new StringBuilder(1024);

            if (exception is SqlException sqlException)
            {
                var innerException = sqlException;

                for (var index = 0; index <= innerException.Errors.Count - 1; index++)
                {
                    exceptionBuilder.AppendLine();
                    exceptionBuilder.AppendLine(new string('*', 40));
                    exceptionBuilder.AppendLine($"**** BEGIN: SQL Server Exception #{index + 1} ****");
                    exceptionBuilder.AppendLine($"     Type: {innerException.Errors[index].GetType().FullName}");
                    exceptionBuilder.AppendLine($"     Message: {innerException.Errors[index].Message}");
                    exceptionBuilder.AppendLine($"     Source: {innerException.Errors[index].Source}");
                    exceptionBuilder.AppendLine($"     Number: {innerException.Errors[index].Number}");
                    exceptionBuilder.AppendLine($"     State: {innerException.Errors[index].State}");
                    exceptionBuilder.AppendLine($"     Class: {innerException.Errors[index].Class}");
                    exceptionBuilder.AppendLine($"     Server: {innerException.Errors[index].Server}");
                    exceptionBuilder.AppendLine($"     Procedure: {innerException.Errors[index].Procedure}");
                    exceptionBuilder.AppendLine($"     LineNumber: {innerException.Errors[index].LineNumber}");
                    exceptionBuilder.AppendLine($"**** END: SQL Server Exception #{index + 1} ****");
                    exceptionBuilder.AppendLine(new string('*', 40));
                }
            }
            else
            {
                exceptionBuilder.Append(exception.Message);
            }

            return exceptionBuilder.ToString();
        }
        #endregion

        #region IsDatabaseSpecificError Method
        protected override bool IsDataBaseSpecificError(Exception exception) => exception is SqlException;
        #endregion
    }
}
