﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AdoDotNet.Common.Common.DataLayer
{
    public static class DataHelper
    {
        public static T GetFieldValue<T>(this IDataReader dataReader, string name)
        {
            T value = default;

            if (!dataReader[name].Equals(DBNull.Value))
                value = (T) dataReader[name];

            return value;
        }
    }
}
