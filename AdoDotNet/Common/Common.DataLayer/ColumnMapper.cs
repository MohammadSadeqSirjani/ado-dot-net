﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace AdoDotNet.Common.Common.DataLayer
{
    public class ColumnMapper
    {
        public string ColumnName { get; set; }

        public PropertyInfo ColumnProperty { get; set; }
    }
}
