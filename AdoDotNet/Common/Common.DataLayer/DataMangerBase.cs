﻿using AdoDotNet.Common.Common.Library.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Reflection.PortableExecutable;
using AdoDotNet;
using AdoDotNet.Common.Common.DataLayer;
using AdoDotNet.Common.Common.Library.Validation;
using DataException = AdoDotNet.Common.Common.DataLayer.DataException;

namespace Common.DataLayer
{
    public abstract class DataManagerBase : CommonBase, IDisposable
    {
        #region Constructor

        public DataManagerBase(string nameOrConnectionString)
        {
            SetConnectionStringOrName(nameOrConnectionString);
            Initialize();
        }
        #endregion

        #region Properties
        private Exception _lastException;

        private int _rowsAffected;

        private string _lastExceptionMessage = string.Empty;

        /// <summary>
        /// Get/Set LastException
        /// </summary>
        public Exception LastException
        {
            get => _lastException;
            set
            {
                _lastException = value;
                LastExceptionMessage = value == null ? string.Empty : value.Message;
                OnPropertyChanged(nameof(LastException));
            }
        }

        /// <summary>
        /// Get/Set LastExceptionMessage
        /// </summary>
        public string LastExceptionMessage
        {
            get => _lastExceptionMessage;
            set
            {
                _lastExceptionMessage = value;
                OnPropertyChanged(nameof(LastExceptionMessage));
            }
        }

        /// <summary>
        /// Get/Set Rows Affected
        /// </summary>
        public int RowsAffected
        {
            get => _rowsAffected;
            set
            {
                _rowsAffected = value;
                OnPropertyChanged(nameof(RowsAffected));
            }
        }

        public IDbCommand Command { get; set; }
        
        public DataSet DataSet { get; set; }
        
        public string ConnectStringName { get; set; }
        
        public string ConnectionString { get; set; }
        
        public object IdentityGenerated { get; set; }
        
        public string ParameterToken { get; set; }
        
        public string SqlCommand { get; set; }
        
        public List<ValidationMessage> ValidationMessages { get; set; }

        public bool IsInTransaction { get; set; }
        #endregion

        #region Initialize Method
        /// <summary>
        /// This method is called by the constructor in the base class
        /// </summary>
        public virtual void Initialize()
        {
            ParameterToken = "@";
            IdentityGenerated = null;
            SqlCommand = string.Empty;
            ValidationMessages = new List<ValidationMessage>();
        }
        #endregion

        #region Reset Method
        public virtual void Reset() => Reset(CommandType.Text);

        public virtual void Reset(CommandType type)
        {
            if (Command != null)
            {
                Command.CommandText = string.Empty;

                Command.CommandType = type;

                Command.Parameters.Clear();
            }

            LastExceptionMessage = string.Empty;

            LastException = null;

            RowsAffected = 0;

            IdentityGenerated = null;

            SqlCommand = string.Empty;

            ValidationMessages.Clear();
        }
        #endregion

        #region CreateParameter Methods
        public abstract IDbDataParameter CreateParameter(string name, object value, bool isNullable);

        public abstract IDbDataParameter CreateParameter(string name, object value, bool isNullable, DbType type, ParameterDirection direction = ParameterDirection.Input);

        public abstract IDbDataParameter CreateParameter(string name, object value, bool isNullable, DbType type, int size, ParameterDirection direction = ParameterDirection.Input);
        #endregion

        #region AddParameter Methods
        public virtual void AddParameter(string name, object value, bool isNullable)
        {
            // Add parameter
            Command.Parameters.Add(CreateParameter(name, value, isNullable));
        }

        public virtual void AddParameter(string name, object value, bool isNullable, DbType type, ParameterDirection direction = ParameterDirection.Input)
        {
            // Add parameter
            Command.Parameters.Add(CreateParameter(name, value, isNullable, type, direction));
        }

        public virtual void AddParameter(string name, object value, bool isNullable, DbType type, int size, ParameterDirection direction = ParameterDirection.Input)
        {
            // Add parameter
            Command.Parameters.Add(CreateParameter(name, value, isNullable, type, size, direction));
        }
        #endregion

        #region GetParameter Method
        public virtual IDataParameter GetParameter(string name)
        {
            if (!name.Contains(ParameterToken))
            {
                name = ParameterToken + name;
            }

            return (IDataParameter)Command.Parameters[name];
        }
        #endregion

        #region GetParameterValue Method
        public abstract T GetParameterValue<T>(string name, object defaultValue);
        #endregion

        #region GetIdentityValue Methods
        public virtual T GetIdentityValue<T>() => GetIdentityValue<T>((T)default);

        public virtual T GetIdentityValue<T>(object defaultValue)
        {
            var value = (T)defaultValue;

            if (IdentityGenerated == null) return value;
            value = (T)Convert.ChangeType(IdentityGenerated, typeof(T));

            return value;
        }
        #endregion

        #region GetRecords Methods
        public virtual List<T> GetRecords<T>(string sql) => GetRecords<T>(sql, CommandType.Text, null);

        public virtual List<T> GetRecords<T>(string sql, params IDbDataParameter[] parameters) => GetRecords<T>(sql, CommandType.Text, parameters);

        public virtual List<T> GetRecords<T>(string sql, CommandType type) => GetRecords<T>(sql, type, null);

        public virtual List<T> GetRecords<T>(string sql, CommandType type, params IDbDataParameter[] parameters)
        {
            // Reset all properties
            Reset(type);

            // Initialize the Sql statement
            SqlCommand = sql;

            // Add any standard parameters
            AddStandardParameters();

            if (parameters != null)
            {
                // Add any custom parameters
                foreach (var parameter in parameters) Command.Parameters.Add(parameter);
            }

            // Execute Query
            using var dr = GetDataReader();
            // Use reflection to load Entity data
            var values = ToList<T>(dr);
            // Set rows returned
            RowsAffected = values.Count;

            // Get standard output parameters
            GetStandardOutputParameters();

            return values;
        }
        #endregion

        #region GetRecordsUsingDataSet Methods
        public virtual List<T> GetRecordsUsingDataSet<T>(string sql)
            => GetRecordsUsingDataSet<T>(sql, CommandType.Text, null);

        public virtual List<T> GetRecordsUsingDataSet<T>(string sql, params IDbDataParameter[] parameters)
            => GetRecordsUsingDataSet<T>(sql, CommandType.Text, parameters);

        public virtual List<T> GetRecordsUsingDataSet<T>(string sql, CommandType type)
            => GetRecordsUsingDataSet<T>(sql, type, null);

        public virtual List<T> GetRecordsUsingDataSet<T>(string sql, CommandType type, params IDbDataParameter[] parameters)
        {
            // Reset all properties
            Reset(type);

            // Assign Sql statement to use
            SqlCommand = sql;

            // Add any standard parameters
            AddStandardParameters();

            if (parameters != null)
            {
                // Add any custom parameters
                foreach (var parameter in parameters) Command.Parameters.Add(parameter);
            }

            // Execute Query
            DataSet = GetDataSet();

            var values = ToList<T>(DataSet.Tables[0]);
            RowsAffected = values.Count;

            // Get standard output parameters
            GetStandardOutputParameters();

            return values;
        }
        #endregion

        #region GetRecord Methods
        public virtual T GetRecord<T>(string sql) where T : new()
            => GetRecord<T>(sql, CommandType.Text, null);

        public virtual T GetRecord<T>(string sql, params IDbDataParameter[] parameters) where T : new()
            => GetRecord<T>(sql, CommandType.Text, parameters);

        public virtual T GetRecord<T>(string sql, CommandType type) where T : new()
            => GetRecord<T>(sql, type, null);

        public virtual T GetRecord<T>(string sql, CommandType type, params IDbDataParameter[] parameters) where T : new()
        {
            var value = new T();

            // Reset all properties
            Reset(type);

            // Create Sql to call stored procedure
            SqlCommand = sql;

            if (parameters != null)
            {
                // Add any custom parameters
                foreach (var parameter in parameters) Command.Parameters.Add(parameter);
            }

            // Add any standard parameters
            AddStandardParameters();

            // Execute Query
            using var dataReader = GetDataReader();
            // Use reflection to load Product data
            var values = ToList<T>(dataReader);
            if (values.Count > 0)
            {
                // Assign return value
                value = values.ElementAt(0);
                RowsAffected = values.Count;
            }

            // Get standard output parameters
            GetStandardOutputParameters();

            return value;
        }
        #endregion

        #region GetRecordUsingDataSet Methods
        public virtual T GetRecordUsingDataSet<T>(string sql) where T : new() 
            => GetRecordUsingDataSet<T>(sql, CommandType.Text, null);

        public virtual T GetRecordUsingDataSet<T>(string sql, params IDbDataParameter[] parameters) where T : new() 
            => GetRecordUsingDataSet<T>(sql, CommandType.Text, parameters);

        public virtual T GetRecordUsingDataSet<T>(string sql, CommandType type) where T : new() 
            => GetRecordUsingDataSet<T>(sql, type, null);

        public virtual T GetRecordUsingDataSet<T>(string sql, CommandType type, params IDbDataParameter[] parameters) where T : new()
        {
            var value = new T();

            // Reset all properties
            Reset(type);

            // Assign Sql statement to use
            SqlCommand = sql;

            // Add any standard parameters
            AddStandardParameters();

            if (parameters != null)
            {
                // Add any custom parameters
                foreach (var parameter in parameters) Command.Parameters.Add(parameter);
            }

            // Execute Query
            DataSet = GetDataSet();
            if (DataSet.Tables.Count > 0)
            {
                var values = ToList<T>(DataSet.Tables[0]);
                if (values.Count > 0)
                {
                    value = values.ElementAt(0);
                    RowsAffected = values.Count;
                }
            }

            // Get standard output parameters
            GetStandardOutputParameters();

            return value;
        }
        #endregion

        #region CountRecords Methods
        public virtual int CountRecords(string sql) => CountRecords(sql, CommandType.Text);

        public virtual int CountRecords(string sql, params IDbDataParameter[] parameters)
            => CountRecords(sql, CommandType.Text, parameters);

        public virtual int CountRecords(string sql, CommandType type)
            => CountRecords(sql, type, null);

        public virtual int CountRecords(string sql, CommandType type, params IDbDataParameter[] parameters)
        {
            // Reset all properties
            Reset(type);

            // Create Sql to count all records
            SqlCommand = sql;

            if (parameters != null)
            {
                // Add any custom parameters
                foreach (var parameter in parameters)
                {
                    Command.Parameters.Add(parameter);
                }
            }

            // Get the count of the records
            RowsAffected = ExecuteScalar<int>();

            return RowsAffected;
        }
        #endregion

        #region SetConnectionStringOrName Method
        public void SetConnectionStringOrName(string nameOrConnectString)
        {
            if (nameOrConnectString.Contains("="))
            {
                ConnectionString = nameOrConnectString;
            }
            else
            {
                ConnectStringName = nameOrConnectString;
                try
                {
                    ConnectionString = Const.ConnectionString;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Can't find appSettings element: " + ConnectStringName, ex);
                }
            }
        }
        #endregion

        #region CreateConnection Methods

        public virtual IDbConnection CreateConnection()
        {
            //SetConnectionString(ConnectStringName);
            return CreateConnection(ConnectionString);
        }

        public abstract IDbConnection CreateConnection(string connectString);
        #endregion

        #region CreateCommand Methods

        public abstract IDbCommand CreateCommand();
        #endregion

        #region CreateDataAdapter Methods

        public abstract DbDataAdapter CreateDataAdapter(IDbCommand command);
        #endregion

        #region BeginTransaction Method
        public IDbTransaction BeginTransaction()
        {
            IsInTransaction = true;
            // Check Command Object
            CheckCommand(Command);

            // Check if connection is open
            if (Command.Connection.State != ConnectionState.Open) Command.Connection.Open();

            // Set transaction
            Command.Transaction = Command.Connection.BeginTransaction();

            return Command.Transaction;
        }
        #endregion

        #region Commit Method
        public void Commit()
        {
            IsInTransaction = false;
            Command.Transaction.Commit();
        }
        #endregion

        #region Rollback Method
        public void Rollback()
        {
            IsInTransaction = false;
            Command.Transaction.Rollback();
        }
        #endregion

        #region CheckCommand Method
        public virtual void CheckCommand(IDbCommand command)
        {
            command ??= CreateCommand();

            command.Connection ??= CreateConnection();

            if (string.IsNullOrEmpty(command.CommandText)) command.CommandText = SqlCommand;
        }
        #endregion

        #region ExecuteScalar Methods

        public virtual T ExecuteScalar<T>(string exceptionMessage = "", T defaultValue = default)
            => ExecuteScalar(Command, exceptionMessage, defaultValue);

        public virtual T ExecuteScalar<T>(IDbCommand command, string exceptionMessage = "", T defaultValue = default)
        {
            var value = defaultValue;

            RowsAffected = 0;
            IdentityGenerated = null;
            try
            {
                // Ensure Command object is correct
                CheckCommand(command);

                // Check for open connection
                var isConnectionOpen = (command.Connection.State == ConnectionState.Open);

                // Open connection if not open
                if (!isConnectionOpen)
                {
                    command.Connection.Open();
                }

                // Execute the Sql
                value = (T)command.ExecuteScalar();

                // Close connection if it was not open originally
                if (!isConnectionOpen)
                {
                    command.Connection.Close();
                }
            }
            catch (Exception exception)
            {
                ThrowDbException(exception, command, exceptionMessage);
            }

            return value;
        }
        #endregion

        #region ExecuteNonQuery Methods
        public virtual int ExecuteNonQuery()
            => ExecuteNonQuery(Command, false, string.Empty, string.Empty);

        public virtual int ExecuteNonQuery(string exceptionMessage)
            => ExecuteNonQuery(Command, false, string.Empty, exceptionMessage);

        public virtual int ExecuteNonQuery(bool retrieveIdentity)
            => ExecuteNonQuery(Command, retrieveIdentity, string.Empty, string.Empty);

        public virtual int ExecuteNonQuery(bool retrieveIdentity, string exceptionMessage)
            => ExecuteNonQuery(Command, retrieveIdentity, string.Empty, exceptionMessage);

        public virtual int ExecuteNonQuery(bool retrieveIdentity, string identityParamName, string exceptionMessage)
            => ExecuteNonQuery(Command, retrieveIdentity, identityParamName, exceptionMessage);

        public virtual int ExecuteNonQuery(IDbCommand command, bool retrieveIdentity = false, string identityParamName = "", string exceptionMessage = "")
        {
            RowsAffected = 0;
            IdentityGenerated = null;
            try
            {
                // Ensure Command object is correct
                CheckCommand(command);

                // Check for open connection
                var isConnectionOpen = (command.Connection.State == ConnectionState.Open);

                // Open connection if not open
                if (!isConnectionOpen)
                {
                    command.Connection.Open();
                }

                if (retrieveIdentity)
                {
                    if (string.IsNullOrEmpty(identityParamName))
                    {
                        // Use a DataSet if retrieving IDENTITY and using Dynamic Sql
                        RowsAffected = ExecuteNonQueryUsingDataSet(command);
                    }
                    else
                    {
                        // Execute the Sql
                        RowsAffected = command.ExecuteNonQuery();

                        // Get output parameter for IDENTITY when using a stored procedure
                        IdentityGenerated = ((IDbDataParameter)command.Parameters[identityParamName]).Value;
                    }
                }
                else
                {
                    // Execute the Sql
                    RowsAffected = command.ExecuteNonQuery();
                }

                // Close connection if it was not open originally
                if (!isConnectionOpen)
                {
                    command.Connection.Close();
                }
            }
            catch (Exception exception)
            {
                ThrowDbException(exception, command, exceptionMessage);
            }

            return RowsAffected;
        }
        #endregion

        #region ExecuteNonQueryUsingDataSet Method
        public int ExecuteNonQueryUsingDataSet(IDbCommand command) 
            => ExecuteNonQueryUsingDataSet(command, string.Empty);

        public int ExecuteNonQueryUsingDataSet(IDbCommand command, string exceptionMessage)
        {
            DataSet = new DataSet();

            RowsAffected = 0;
            IdentityGenerated = null;

            command.CommandText += ";SELECT @@ROWCOUNT As RowsAffected, SCOPE_IDENTITY() AS IdentityGenerated";
            try
            {
                using var dataAdapter = CreateDataAdapter(command);
                dataAdapter.Fill(DataSet);
                if (DataSet.Tables.Count > 0)
                {
                    RowsAffected = (int)DataSet.Tables[0].Rows[0]["RowsAffected"];
                    IdentityGenerated = DataSet.Tables[0].Rows[0]["IdentityGenerated"];
                }
            }
            catch (Exception exception)
            {
                ThrowDbException(exception, command, exceptionMessage);
            }

            return RowsAffected;
        }
        #endregion

        #region GetDataSet Methods

        public virtual DataSet GetDataSet() => GetDataSet(Command, string.Empty);

        public virtual DataSet GetDataSet(string exceptionMessage) => GetDataSet(Command, exceptionMessage);

        public virtual DataSet GetDataSet(IDbCommand command, string exceptionMessage = "")
        {
            DataSet = new DataSet();

            try
            {
                // Ensure Command object is correct
                CheckCommand(command);
                // Create SqlDataAdapter
                using var dataAdapter = CreateDataAdapter(command);
                // Fill the DataSet
                dataAdapter.Fill(DataSet);
            }
            catch (Exception exception)
            {
                ThrowDbException(exception, command, exceptionMessage);
            }

            return DataSet;
        }
        #endregion

        #region GetDataReader Methods

        public virtual IDataReader GetDataReader() => GetDataReader(Command, string.Empty);

        public virtual IDataReader GetDataReader(string exceptionMessage = "") => GetDataReader(Command, exceptionMessage);

        public virtual IDataReader GetDataReader(IDbCommand command, string exceptionMessage = "")
        {
            IDataReader dataReader = null;

            try
            {
                // Ensure Command object is correct
                CheckCommand(command);
                // Open Connection
                command.Connection.Open();
                // Create DataReader
                dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception exception)
            {
                ThrowDbException(exception, command, exceptionMessage);
            }

            return dataReader;
        }
        #endregion

        #region AddStandardParameters

        public abstract void AddStandardParameters();
        #endregion

        #region GetStandardOutputParameters Method

        public abstract void GetStandardOutputParameters();
        #endregion

        #region AddValidationMessage Method
        public ValidationMessage AddValidationMessage(string propertyName, string message)
        {
            var value = new ValidationMessage
            {
                PropertyName = propertyName,
                Message = message
            };

            ValidationMessages.Add(value);

            return value;
        }
        #endregion

        #region Validate Method       
        /// <summary>
        /// Override this method to validate your entity object
        /// </summary>
        /// <param name="entityToValidate">The entity to validate</param>
        /// <returns>True if entity is valid</returns>
        public virtual bool Validate<T>(T entityToValidate)
        {
            var propName = string.Empty;
            ValidationMessages.Clear();

            if (entityToValidate == null) return ValidationMessages.Count > 0;
            var context = new ValidationContext(entityToValidate, null, null);
            var results = new List<ValidationResult>();

            if (Validator.TryValidateObject(entityToValidate, context, results, true))
                return (ValidationMessages.Count > 0);
            foreach (var item in results)
            {
                if (((string[]) item.MemberNames).Length > 0) propName = ((string[]) item.MemberNames)[0];
                ValidationMessages.Add(new ValidationMessage
                {
                    Message = item.ErrorMessage,
                    PropertyName = propName
                });
            }

            return ValidationMessages.Count > 0;
        }
        #endregion

        #region ToList Methods

        public virtual List<T> ToList<T>(IDataReader reader)
        {
            var values = new List<T>();
            var type = typeof(T);
            var columns = new List<ColumnMapper>();
            string name;

            // Get all the properties in Entity Class
            var props = type.GetProperties();

            // Get all ColumnAttribute's
            var attributes = props.Where(p => p.GetCustomAttributes(typeof(ColumnAttribute), false).Any()).ToArray();

            // Loop through one time to map columns to properties
            // NOTES:
            //   Assumes your column names are the same name as your class property names
            //   or the [Column()] attribute is used on your class
            //
            //   Any properties not in the data reader column list are not set
            //   Could implement caching here based on the Type passed in so you only need to perform this loop once
            for (var index = 0; index < reader.FieldCount; index++)
            {
                // Get name from data reader
                name = reader.GetName(index);
                // See if column name maps directly to property name
                var columnInfo = props.FirstOrDefault(c => c.Name == name);

                // Column!=Property -> See if the column name is in a ColumnAttribute
                if (columnInfo == null)
                {
                    foreach (var attribute in attributes)
                    {
                        var template = attribute.GetCustomAttribute(typeof(ColumnAttribute));
                        if (template == null || ((ColumnAttribute)template).Name != name) continue;
                        columnInfo = props.FirstOrDefault(c => c.Name == attribute.Name);
                        break;
                    }
                }

                if (columnInfo != null)
                {
                    columns.Add(new ColumnMapper
                    {
                        ColumnName = name,
                        ColumnProperty = columnInfo
                    });
                }
            }

            // Loop through all records
            while (reader.Read())
            {
                // Create new instance of Entity
                var entity = Activator.CreateInstance<T>();

                // Loop through columns to assign data
                foreach (var column in columns)
                {
                    column.ColumnProperty.SetValue(entity,
                        reader[column.ColumnName].Equals(DBNull.Value)
                            ? null
                            : reader[column.ColumnName], null);
                }

                values.Add(entity);
            }

            return values;
        }

        public virtual List<T> ToList<T>(DataTable dataTable)
        {
            var values = new List<T>();
            var type = typeof(T);
            var columns = new List<ColumnMapper>();
            string name;

            // Get all the properties in Entity Class
            var props = type.GetProperties();

            // Get all ColumnAttribute's
            var attributes = props.Where(p => p.GetCustomAttributes(typeof(ColumnAttribute), false).Any()).ToArray();

            // Loop through one time to map columns to properties
            // NOTES:
            //   Assumes your column names are the same name as your class property names
            //   or the [Column()] attribute is used on your class
            //
            //   Any properties not in the data set column list are not set
            //   Could implement caching here based on the Type passed in so you only need to perform this loop once
            for (var index = 0; index < dataTable.Columns.Count; index++)
            {
                // Get name from data reader
                name = dataTable.Columns[index].ColumnName;
                // See if column name maps directly to property name
                var columnInfo = props.FirstOrDefault(c => c.Name == name);

                // Column!=Property -> See if the column name is in a ColumnAttribute
                if (columnInfo == null)
                {
                    foreach (var attribute in attributes)
                    {
                        var template = (ColumnAttribute) attribute.GetCustomAttribute(typeof(ColumnAttribute));
                        if (template == null || template.Name != name) continue;
                        columnInfo = props.FirstOrDefault(c => c.Name == attribute.Name);
                        break;
                    }
                }

                if (columnInfo != null)
                {
                    columns.Add(new ColumnMapper
                    {
                        ColumnName = name,
                        ColumnProperty = columnInfo
                    });
                }
            }

            // Loop through all records
            for (var rows = 0; rows < dataTable.Rows.Count; rows++)
            {
                // Create new instance of Entity
                var entity = Activator.CreateInstance<T>();

                // Loop through columns to assign data
                foreach (var column in columns)
                {
                    column.ColumnProperty.SetValue(entity,
                        dataTable.Rows[rows][column.ColumnName].Equals(DBNull.Value)
                            ? null
                            : dataTable.Rows[rows][column.ColumnName], null);
                }

                values.Add(entity);
            }

            return values;
        }
        #endregion

        #region ThrowDbException Method

        public virtual void ThrowDbException(Exception exception, IDbCommand command, string exceptionMessage = "")
        {
            // Set the last exception
            LastException = CreateDbException(exception, command, exceptionMessage);

            throw LastException;
        }
        #endregion

        #region CreateDbException Method
        public virtual DataException CreateDbException(Exception exception, IDbCommand command, string exceptionMessage = "")
        {
            exceptionMessage = string.IsNullOrEmpty(exceptionMessage) ? string.Empty : exceptionMessage + " - ";

            var dataException = new DataException($"{exceptionMessage}{exception.Message}", exception)
            {
                ConnectionString = command.Connection.ConnectionString,
                ConnectionStringInAppSetting = ConnectStringName,
                DataBase = command.Connection.Database,
                Sql = SqlCommand,
                CommandParameters = command.Parameters,
                WorkStationId = Environment.MachineName
            };

            return dataException;
        }
        #endregion

        #region Dispose Method
        public virtual void Dispose()
        {
            if (Command == null) return;
            if (Command.Connection != null)
            {
                Command.Transaction?.Dispose();
                Command.Connection.Close();
                Command.Connection.Dispose();
            }
            Command.Dispose();
        }
        #endregion
    }
}