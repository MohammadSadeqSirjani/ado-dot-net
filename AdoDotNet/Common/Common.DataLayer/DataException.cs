﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace AdoDotNet.Common.Common.DataLayer
{
    public class DataException : Exception
    {
        public DataException() { }

        public DataException(string message) : base(message) { }

        public DataException(string message, Exception innerException) : base(message, innerException) { }

        public IDataParameterCollection CommandParameters { get; set; }

        public string Sql { get; set; }

        private string _connectionString = string.Empty;

        public string ConnectionString
        {
            get => HideLoginInfoForConnectionString(_connectionString);
            set => _connectionString = value;
        }

        public string ConnectionStringInAppSetting { get; set; }

        public string DataBase { get; set; }

        public string WorkStationId { get; set; }

        protected string HideLoginInfoForConnectionString(string connectionString)
        {
            connectionString = connectionString.Trim();

            if (connectionString.Length <= 0) return connectionString;
            if (!connectionString.EndsWith(";"))
            {
                connectionString += ";";
            }

            var values = connectionString.Split(";");

            for (var index = 0; index < values.Length - 1; index++)
            {
                if (values[index].ToLower().IndexOf("uid", StringComparison.Ordinal) >= 0)
                {
                    values[index] = $"uid={new string('*', 10)}";
                }

                if (values[index].ToLower().IndexOf("user id=", StringComparison.Ordinal) >= 0)
                {
                    values[index] = $"user id={new string('*', 10)}";
                }

                if (values[index].ToLower().IndexOf("pwd=", StringComparison.Ordinal) >= 0)
                {
                    values[index] = $"pwd={new string('*', 10)}";
                }

                if (values[index].ToLower().IndexOf("password=", StringComparison.Ordinal) >= 0)
                {
                    values[index] = $"password={new string('*', 10)}";
                }
            }

            connectionString = string.Join(";", values);

            return connectionString;
        }

        protected virtual string GetCommandParametersAsString()
        {
            var commandParams = new StringBuilder(1024);

            if (CommandParameters == null || CommandParameters.Count < 0) return commandParams.ToString();
            commandParams = new StringBuilder(1024);

            foreach (IDbDataParameter parameter in CommandParameters)
            {
                commandParams.Append($" {parameter.ParameterName}");
                if (parameter.Value == null)
                    commandParams.AppendLine(" = null");
                commandParams.AppendLine($" = {parameter.Value}");
            }

            return commandParams.ToString();
        }

        protected virtual string GetDataBaseSpecificError(Exception exception)
        {
            var exceptionBuilder = new StringBuilder(1024);

            switch (exception)
            {
                case SqlException sqlException:
                    {
                        var innerException = sqlException;

                        for (var index = 0; index <= innerException.Errors.Count - 1; index++)
                        {
                            exceptionBuilder.AppendLine();
                            exceptionBuilder.AppendLine(new string('*', 40));
                            exceptionBuilder.AppendLine($"**** BEGIN: SQL Server Exception #{index + 1} ****");
                            exceptionBuilder.AppendLine($"     Type: {innerException.Errors[index].GetType().FullName}");
                            exceptionBuilder.AppendLine($"     Message: {innerException.Errors[index].Message}");
                            exceptionBuilder.AppendLine($"     Source: {innerException.Errors[index].Source}");
                            exceptionBuilder.AppendLine($"     Number: {innerException.Errors[index].Number}");
                            exceptionBuilder.AppendLine($"     State: {innerException.Errors[index].State}");
                            exceptionBuilder.AppendLine($"     Class: {innerException.Errors[index].Class}");
                            exceptionBuilder.AppendLine($"     Server: {innerException.Errors[index].Server}");
                            exceptionBuilder.AppendLine($"     Procedure: {innerException.Errors[index].Procedure}");
                            exceptionBuilder.AppendLine($"     LineNumber: {innerException.Errors[index].LineNumber}");
                            exceptionBuilder.AppendLine($"**** END: SQL Server Exception #{index + 1} ****");
                            exceptionBuilder.AppendLine(new string('*', 40));
                        }

                        break;
                    }
                default:
                    exceptionBuilder.Append(exception.Message);
                    break;
            }

            return exceptionBuilder.ToString();
        }

        protected virtual bool IsDataBaseSpecificError(Exception exception) => exception is SqlException;

        protected virtual string GetInnerExceptionInfo()
        {
            var stringBuilder = new StringBuilder();
            var innerException = InnerException;
            var index = 1;

            while (innerException != null)
            {
                if (IsDataBaseSpecificError(innerException))
                    stringBuilder.Append(GetDataBaseSpecificError(innerException));
                stringBuilder.AppendLine(new string('*', 40));
                stringBuilder.AppendLine($"**** BEGIN: Inner Exception #{index} ****");
                stringBuilder.AppendLine($"     Type: {innerException.GetType().FullName}");
                stringBuilder.AppendLine($"     Message: {innerException.Message}");
                stringBuilder.AppendLine($"     Source: {innerException.Source}");
                stringBuilder.AppendLine($"     Trace: {innerException.StackTrace}");
                stringBuilder.AppendLine($"**** END: Inner Exception #{index} ****");
                stringBuilder.AppendLine(new string('*', 40));

                index++;
                innerException = innerException.InnerException;
            }

            return stringBuilder.ToString();
        }

        public override string ToString()
        {
            var dataException = new StringBuilder(1024);

            dataException.AppendLine(new string('-', 80));
            if (!string.IsNullOrEmpty(Message)) dataException.AppendLine($"Type: {GetType().FullName}");
            if (!string.IsNullOrEmpty(DataBase)) dataException.AppendLine($"Message: {DataBase}");
            if (!string.IsNullOrEmpty(Sql)) dataException.AppendLine($"Sql: {Sql}");
            if (CommandParameters.Count > 0) dataException.AppendLine($"Parameters: {GetCommandParametersAsString()}");
            if (!string.IsNullOrEmpty(ConnectionString))
                dataException.AppendLine($"Connection String: {ConnectionString}");
            if (!string.IsNullOrEmpty(ConnectionStringInAppSetting))
                dataException.AppendLine(
                    $"Connect String Name in Config File (<appSettings>): '{ConnectionStringInAppSetting}'");
            if (!string.IsNullOrEmpty(StackTrace)) dataException.AppendLine($"Stack Trace: {StackTrace}");
            if (IsDataBaseSpecificError(this)) dataException.AppendLine(GetDataBaseSpecificError(this));
            // Gather info from inner exceptions
            dataException.AppendLine(GetInnerExceptionInfo());

            return dataException.ToString();
        }
    }
}
